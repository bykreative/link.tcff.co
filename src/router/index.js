import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Track from '@/components/Track'
import Customize from '@/components/Customize'

Vue.use(Router)

export default new Router({
  //mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/track',
      name: 'Track',
      component: Track
    },
    {
      path: '/customize',
      name: 'Customize',
      component: Customize
    }
  ]
})
